# password-validator

![my_devops](my_devops.jpg?raw=true?style=centerme "hand drawn by me")

## Sela project week 2:

Password validation using bash.
[feature] branch - same script with the anbility to retrieve password from a file.
[powershell] branch - same script for powershell

## plan for password-validator.sh

- recheck exit codes check - how can we check if a script file ends with 0?
- Toolbox - variables declarations globals ETC.
- validate function that runs over both checks (with / without file)

# QA1:

- check with a differtent path and custom file - too long etc.
- readme file - prettify

## - QA2 - make sure nothing is left out such as length:

    1. length recheck all validations.
    2. enforce same standards - such as double qoutes.
    3. proper identation for bash file.
    4. best practices and refractor? // is the regex better then grep -E ?

<!-- - improve? add speacial characters? -->

moving forward for better practice reasons have decided to go with regexes even if it means using them as strings in if else blocks.

complete bonus - [pwsh]
document and rebuild plan
explain why you did stuff (:
study heredoc

DOCS-

len=${#1} # shortcut for length

1.check for supplied arg

debate the structure:
toolbox
validate function ()=> if else
bare switch case ()=> validate function

CLI -->
echo '#! /bin/bash' > password-validator.sh
sudo chmod +x ./password-validator.sh

- Feature branch
  MyP@ssw0rd! > password.txt

## git alias ~/.bash_aliases | E | source ~/.bash_aliases

alias gp='git push origin master' /n
alias gc='git commit -am ' /n
alias gcc='git add -A && git push' /n
alias gco='git checkout -amb' /n
alias gs='git status'

[[sources]]:
color terminal:

- https://gist.github.com/vratiu/9780109
  ubuntu alias:
- https://www.cyberciti.biz/faq/create-permanent-bash-alias-linux-unix/
- https://tldp.org/LDP/abs/html/exit-status.html
- https://askubuntu.com/questions/474556/hiding-output-of-a-commandcheck-the-last-exit-code-of-a-command-echoed-by-a-bash-script

[[Archive]]

<!-- ## check out my older version at -archive: password-validator_dep.sh -->
<!-- fail change: itirating the tests on one function! -->

## originally i had switch case and regex, reverted to echo so exit tests could be applied
