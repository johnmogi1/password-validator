#! /bin/bash
min_length=10
Red='\033[0;31m'        # Red
Green='\033[0;32m'      # Green
NC='\033[0m '           # No Color

# exit the program if no argument supplied:
if [[ ${#1} -lt min_length ]]; then
printf "${Red}Try again... \nPassword on file is too short, at least 10 characters${NC}\n"
exit 1;
fi

# validate function tests password string for complexity: 

validate (){ 
echo "$1" | grep -E '.*[0-9]' > /dev/null 2>&1
if [[ $? -eq 1 ]] ; then
    printf "${Red}Try again... \nPassword must contain at least one number${NC}\n"
    exit 1
fi

echo "$1" | grep -E '.*[a-z]' > /dev/null 2>&1
if [[ $? -eq 1 ]] ; then
    printf "${Red}Try again... \nPassword must contain at least one small letter${NC}\n"
    exit 1
fi

echo "$1" | grep -E '.*[A-Z]' > /dev/null 2>&1
if [[ $? -eq 1 ]] ; then
    printf "${Red}Try again... \nPassword must contain at least one capital letter${NC}\n"
    exit 1
fi
}

validate $1

printf "${Green}success${NC}password is secured\n"
